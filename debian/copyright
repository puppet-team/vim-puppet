Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: vim-puppet
Source: https://github.com/rodjek/vim-puppet

Files: *
Copyright: (c) 2018 Tim Sharpe <tim@sharpe.id.au>
License: Apache-2.0

Files: syntax/puppet.vim
Comment: Origins of puppet.vim can be traced back to the puppet source tree. See
 https://github.com/rodjek/vim-puppet/commit/bdf19c3644f45bad89d44ba2d62471fe30e43639
Copyright: (c) 2005-2010 Puppet Labs Inc
           (c) 2018 Tim Sharpe <tim@sharpe.id.au>
License: Apache-2.0

Files: debian/*
Copyright: (c) 2018 Apollon Oikonomopoulos <apoikos@debian.org>
           (c) 2010-2012 Stig Sandbeck Mathisen <ssm@debian.org>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache License, Version 2.0 can be
 found in /usr/share/common-licenses/Apache-2.0.
